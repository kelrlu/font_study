from django.http import *
from django.template import loader
from django.contrib.auth import load_backend,login,logout
from django.conf import settings
from .forms import *
import datetime
from django.shortcuts import render
from django.template import RequestContext
import pandas as pd
import os 
from .settings import PROJECT_ROOT
import random
#from .models import *
import itertools
import urllib
import re
import csv,sqlite3

# main instruction page rendering 
def index(request):
	template = loader.get_template('index.html')
	context = {}
	return HttpResponse(template.render(context,request))

# study functions

# partitions words by section
def word_section(words,word_configs,stories,fonts):
	print 'shuffling words randomly'
	random.shuffle(words)

	# combine words and word_configs
	wwc = [[x,y[0],y[1]] for x,y, in zip(words, word_configs)]
	
	print 'words', wwc
	s1_raw = wwc[:30]
	s1 = [s1_raw[:10],s1_raw[10:20],s1_raw[20:],[stories[0],fonts[0]]]
	print 'S1', s1

	s2_raw = wwc[30:60]
	s2 = [s2_raw[:10],s2_raw[10:20],s2_raw[20:],[stories[1],fonts[1]]]
	print 'S2',s2

	# s3_raw = wwc[60:90]
	# s3 = [s3_raw[:10],s3_raw[10:20],s3_raw[20:],[stories[2],fonts[2]]]
	# print 'S3',s3, len(s3)

	# s4_raw = wwc[90:120]
	# s4 = [s4_raw[:10],s4_raw[10:20],s4_raw[20:],[stories[3],fonts[3]]]
	# print 's4', len(s4)

	# return [s1,s2,s3,s4,wwc]
	return [s1,s2,wwc]

# gets array of word configurations
def get_word_configs():
	fonts = ['Monotype Corsiva', 'Comic Sans MS', 'Verdana', 'Times New Roman']
	sizes = [25, 50, 75]
	configs = map(lambda x:(x[0],x[1]),itertools.chain(itertools.product(fonts, sizes), itertools.product(fonts, sizes)))
	word_configs = list(map(lambda x:random.choice(configs), [0] * 120))
	# for stories
	random.shuffle(fonts)
	return word_configs, configs,fonts

	# read in words from excel file
def get_words():
	print 'SERVER'
	print 'PATH',os.path.join(PROJECT_ROOT,'words.xlsx')
	wdata = pd.read_excel(os.path.join(PROJECT_ROOT,'words.xlsx'))
	print 'READ EXCEL'
	all_words = list(map(lambda x: x.strip().encode('utf-8'), wdata[wdata.columns.values[1]]))

	print 'ALL WORDS', len(all_words)
	return all_words[:120]

def get_stories():
	stories = []
	story = None
	story_files = ['font_study/story1.txt','font_study/story2.txt']
	for story_file in story_files:
		with open(story_file,'r') as f:
			title = f.readline()
			story = f.read()
		stories.append([title,story])
	print 'STORIES', stories
	return stories


def parse_recognized_words(recognized_words_raw):
	print 'IN PARSING RECOGNIZED WORDS'
	### re.split(r'[,;]', recognized_words_raw)
	words = list(map(lambda x: x.strip().lower(), re.split(',| ',recognized_words_raw)))
	words = list(filter(lambda x: len(x)!=0 , words))
	print 'PARSED RECOGNIZED WORDS:', words
	return words

# study master function
# section 1,2,3,4 refer to sections of words
# part 1,2,3 are flashing word sections, part 4 is the story
def study(request):
	# retrieve session information 
	s1 = request.session['s1']
	s2 = request.session['s2']
	# s3 = request.session['s3']
	# s4 = request.session['s4']
	rw = request.session['rw']
	section = request.GET.get('section').strip()
	part = request.GET.get('part')

	print 'SECTION ', section
	print 'PART', part

	print 'S2', s2[3][0]
	print 'S2 story', (s2[3][0][1]).encode('utf-8')

	story = None
	if section == '1': 
		story_title = s1[3][0][0]
		story = s1[3][0][1]
		story_font = s1[3][1]
		if part == '1':
			sec = s1[0]
		elif part == '2':
			sec = s1[1]
		elif part == '3':
			sec = s1[2]
		else:
			sec = None
	elif section == '2':
		story_title = s2[3][0][0]
		story = s2[3][0][1].encode('utf-8')
		story_font = s2[3][1]
		if part == '1':
			sec = s2[0]
		elif part == '2':
			sec = s2[1]
		elif part == '3':
			sec = s2[2]
		else:
			sec = None
	# elif section == '3':
	# 	story_title = s3[3][0][0]
	# 	story = s3[3][0][1]
	# 	story_font = s3[3][1]
	# 	if part == '1':
	# 		sec = s3[0]
	# 	elif part == '2':
	# 		sec = s3[1]
	# 	elif part == '3':
	# 		sec = s3[2]
	# 	else:
	# 		sec = None
	# elif section == '4':
	# 	story_title = s4[3][0][0]
	# 	story = s4[3][0][1]
	# 	story_font = s4[3][1]
	# 	if part == '1':
	# 		sec = s4[0]
	# 	elif part == '2':
	# 		sec = s4[1]
	# 	elif part == '3':
	# 		sec = s4[2]
	# 	else:
	# 		sec = None

		print 'STORY AND STORY FONT:', story, story_font
		print 'PART NUMBER', part
	template = loader.get_template('study.html')
	# context = {'sec': sec, 's1':s1, 's2':s2,'s3':s3,'s4':s4,'rw':[],'story_title':story_title, 'story':story,'story_font':story_font, 'section':section,'part':part}
	context = {'sec': sec, 's1':s1, 's2':s2,'rw':[],'story_title':story_title, 'story':story,'story_font':story_font, 'section':section,'part':part}
	return HttpResponse(template.render(context,request))


def get_next_part(section,part):
	if section==2 and part ==4:
		return [int(section), int(part), True]
	else:
		if(part != 4):
			return [int(section), int(part) + 1,False]
		else:
			return [int(section) + 1, 1,False]
		

def next_part(request):
	print 'Moving onto next part'
	recognized_words = []
	if request.method == 'POST':
		next_part_form = NextPartForm(request.POST)
		if next_part_form.is_valid():
			print 'VALID'
			section = next_part_form.cleaned_data['section']
			part = next_part_form.cleaned_data['part']

			recognized_words_raw = next_part_form.cleaned_data['recognized_words']
			recognized_words = parse_recognized_words(recognized_words_raw)
		else:
			print 'NOT VALID'
			next_part_form = NextPartForm()

		print 'cur sec = ', section
		print 'cur part =', part
		print 'next sec = ', int(section) + 1

		# set session variables we're tracking
		request.session['rw'] = request.session['rw'] + recognized_words
		print 'NEW REC WORDS:', request.session['rw']
		# move to next session
		next_section, next_part, done = get_next_part(section,part)
		if(done):
			url = '/finish'
		else:
			url = '/study/?' + urllib.urlencode({'section': str(next_section), 'part': str(next_part)})
		print 'URL REDIRECT', url
		return HttpResponseRedirect(url)

# does analysis for us regarding statistics and what not
def analyze(recognized_words, all_words):
	return 0
	
# writes data to files for our analysis
def write_data(uid, counter_dict,recognized_words,story_fonts,wrong_words):
	# # write to master CSV file - cannot do because of file writing error
	# line = str(uid) + 
	# with open('all.csv','a+') as a:
	# 	a.write(line + "\n")

	# key to access values
	keys = counter_dict.keys()
	total = list(map(lambda x:str(counter_dict[x][1]),keys))
	recognized = list(map(lambda x:str(counter_dict[x][0]),keys))
	percent_recognized = list(map(lambda x,y:str(float(x)/int(y)), recognized,total))
	# keys to write out
	write_out_keys = list(map(lambda x:str(x[0]) + " " + str(x[1]),keys))

	print 'KEYS',keys
	print 'TOTAL',total
	print 'RECGONIZED',recognized
	print 'percent_recognized', percent_recognized
	print 'WRITE OUT KEYS', write_out_keys

	print 'DATA ALERT'
	print ',' + ','.join(write_out_keys) + ",story1font,story2font,number wrong words" + "\n"
	print uid + "," + ','.join(percent_recognized) + "," + story_fonts[0] + "," + story_fonts[1] + "," + str(len(wrong_words))
	# # write to individual, detailed file 
	# with open('font_study/data/' + uid +'.txt','w+') as a:
	# 	a.write('Participant:' + uid + "\n\n")
	# 	a.write('Combinations,' + ','.join(write_out_keys) + "\n")
	# 	a.write('Total Tested,' + ','.join(total) + "\n")
	# 	a.write('Total Recognized,' + ','.join(recognized) + "\n")

	# 	a.write('\nRecognized Words:\n' + "\n".join(recognized_words)+ "\n")
	# 	a.write('\nStory Fonts:\n' + "\n".join(story_fonts[0:2]) + "\n")

	# # get percentage recognized

	# #write to csv file
	# with open('font_study/data/' + uid + ".csv",'w+') as c:
	# 	c.write(',' + ','.join(write_out_keys) + ",story1font,story2font,number wrong words" + "\n")
	# 	c.write(uid + "," + ','.join(percent_recognized) + "," + story_fonts[0] + "," + story_fonts[1] + "," + str(len(wrong_words)) + "\n")

# 
def finish(request):
	print 'IN FINISH'
	# collect data here and write to file
	uid = request.session['uid']
	print 'UID',uid
	recognized_words = list(map(lambda x:x.encode('utf-8'),request.session['rw']))
	print 'RECGONISTED', recognized_words
	wwc = request.session['wwc']
	print 'ALL WORDS WWC',wwc
	configs = request.session['configs']
	print 'CONFIGS',configs
	counter_dict = {}
	for config in configs:
		counter_dict[(config[0].encode('utf-8'),config[1])] = [0,0] # recognized / total

	print 'ZERO DICT', counter_dict
	for word in wwc:
		print 'WRORRRRRDD', word
		if(word[0] in recognized_words):
			counter_dict[(word[1],word[2])][0] += 1
		counter_dict[(word[1],word[2])][1] += 1

	story_fonts = request.session['story_fonts']
	print 'NONZERO DICT', counter_dict

	wrong_words = []
	for word in recognized_words:
		print 'LOOKING AT WRONG WORD',word
		if word not in wwc:
			wrong_words.append(word)
	print 'WRONG WORDS', wrong_words
	print 'BEFORE WRITE DATA'
	write_data(uid, counter_dict, recognized_words,story_fonts,wrong_words)

	# logout 
	request.session['uid'] = None
	# load finish template
	template = loader.get_template('finish.html')
	context = {}
	return HttpResponse(template.render(context,request))


# login user 
def login(request):
	template = loader.get_template('login.html')
	context = {}
	return HttpResponse(template.render(context,request))


def logging_in(request):
	print 'LOGGING IN'
	uid = None
	if request.method == 'POST':
		user_form = UserForm(request.POST)
		print 'USER ID', user_form.data['uid']
		if user_form.is_valid():
			uid = user_form.cleaned_data['uid']
		else:
			user_form = UserForm()
			
		# Set up login information
		request.session['uid'] = uid
		print 'HERE'
		# Get necessary information - set up session for the user
		all_words = get_words()
		word_configs,configs,fonts = get_word_configs()
		stories = get_stories()
		# s1,s2,s3,s4,wwc = word_section(all_words,word_configs,stories,fonts)
		s1,s2,wwc = word_section(all_words,word_configs,stories,fonts)
		request.session['s1'] = s1
		request.session['s2'] = s2
		# request.session['s3'] = s3
		# request.session['s4'] = s4
		request.session['rw'] = [] #recognized words 
		request.session['wwc'] = wwc
		request.session['configs'] = configs
		request.session['story_fonts'] = fonts

		
		url = '/study/?' + urllib.urlencode({'section': '1', 'part': '1'})
		print 'URL REDIRECT', url
		return HttpResponseRedirect(url)
