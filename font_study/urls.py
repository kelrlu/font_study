"""font_study URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin

from django.contrib.auth import views as auth_views

from django.shortcuts import render
from font_study import views 
# from .models import Question

urlpatterns = [
	url(r'^login/$',views.login,name='login'),
    url(r'^logging_in/$',views.logging_in, name ='logging_in'),
	url(r'^logout/$',auth_views.logout,name='logout'),
	url(r'^$', views.index,name='index'),
	# url(r'^study/(?P<section>[1-4])/(?P<part>[1-4])$',views.study,name='study'),
    url(r'^study/$',views.study,name='study'),
    url(r'^next_part/$',views.next_part,name='next_part'),
    url(r'^finish',views.finish,name='finish'),
    url(r'^admin', admin.site.urls),
]
