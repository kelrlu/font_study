import os
from django.core.wsgi import get_wsgi_application
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "font_study.settings")
# from dj_static import Cling
# application = Cling(get_wsgi_application())

import django.core.handlers.wsgi
application = django.core.handlers.wsgi.WSGIHandler()