from django import forms

class UserForm(forms.Form):
	uid = forms.CharField(max_length=256)

class NextPartForm(forms.Form):
	section = forms.IntegerField()
	part = forms.IntegerField()
	recognized_words = forms.CharField(required=False,max_length=1000)
