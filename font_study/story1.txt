The Goldilocks Thief


A couple from Lancashire returned from their holiday to discover a burglar fast asleep in their bed. The retired couple were amazed to find the intruder, Lukasz Chojnowski, had made himself some meals, washed his socks and underwear, tidied the house, and had done a grocery shop.
Mr Holtby said they were not threatened by their uninvited guest. He said he rang police and Chojnowski was still asleep when officers came to arrest him.
Chojnowski, 28, was given a two-year conditional discharge and ordered to pay £200 costs at Burnley Crown Court after admitting burglary. Ms Dyson said she felt "fairly sympathetic although he did break in... but it was neatly done..it was a bit like [Goldilocks] and the Three Bears...although he was more like Baby Bear."
